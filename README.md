[![pipeline status][pipeline_status]](https://gitlab.com/milkpirate/yeet/commits/master)
[![coverage report][code_coverage]](https://gitlab.com/milkpirate/yeet/commits/master)

[![made-with-python][python_badge]](https://www.python.org/) 
[![made-with-sphinx-doc][sphinx_badge]](https://www.sphinx-doc.org/)

[![Version][version_badge]]()
[![GitHub license][license_badge]](https://gitlab.com/milkpirate/yeet/blob/master/LICENSE)
[![Open Source Love][open_source_badge]](https://github.com/ellerbrock/open-source-badges/)

# YEE(light)T(ool)
This is a small python base CLI to control Xiaomi Yeelights.

## Install & Run
```bash
$ cd src
[$ python3 -m venv venv3]
[$ source venv3/bin/activate]
$ pip install -r requirements.txt
$ ./yeet --help
```

## Note
Developer mode needs to be enabled on all Yeelights you want to control.

[pipeline_status]: https://gitlab.com/milkpirate/yeet/badges/master/pipeline.svg "pipeline_status"
[code_coverage]: https://gitlab.com/milkpirate/yeet/badges/master/coverage.svg "code coverage"
[python_badge]: https://img.shields.io/badge/made%20with-Python-1f425f.svg "python badge"
[sphinx_badge]: https://img.shields.io/badge/made%20with-Sphinx-1f425f.svg "sphinx badge"
[version_badge]: https://img.shields.io/badge/version-1.0.0-blue.svg "version bagde"
[license_badge]: https://img.shields.io/badge/license-GPL%20v3-blue.svg "version bagde"
[open_source_badge]: https://badges.frapsoft.com/os/v2/open-source.svg?v=103 "version bagde"