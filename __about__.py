#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

from __version__ import __version__, __release__

__all__ = [
    "__title__",
    "__summary__",
    "__uri__",
    "__version__",
    "__release__",
    "__author__",
    "__email__",
    "__license__",
]

__title__ = "yeet"
__summary__ = "Small CLI to control Xiaomi Yeelights"
__uri__ = 'https://gitlab.com/milkpirate/yeet'

__author__ = "Paul Schroeder"
__email__ = ""

__license__ = 'GPLv3'
