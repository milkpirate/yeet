YEE(light multi)T(ool)
======================

This is a executable to control XIAOMI Yeelights. To make this work flawlessly
the developer mode must be activated on all Yeelights you want to control. For
details see CLI documentation below.

.. literalinclude:: ../../src/yeet/yeet.py
   :start-after: """
   :end-before: """

.. automodule:: yeet
   :undoc-members:
   :members:
   :show-inheritance:
