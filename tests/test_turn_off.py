#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pytest

import queue
import socket

# mock
import yeelight
# module under test
from conftest import yeet


@pytest.mark.parametrize("ret_msg, exp_ret_code", [
    pytest.param("ok",  0),
    pytest.param("Ok",  0),
    pytest.param("OK",  0),
    pytest.param("nok", 3),
])
def test_turn_off_conn_ok(ret_msg, exp_ret_code):
    yeelight.turn_off_ret = ret_msg
    assert yeet.turn_off("some host") == exp_ret_code, "wrong ret code"

    # check that bulb was correctly set up
    assert yeelight.bulb_ip.get() == "IP", "mock ip not 'IP'"
    assert yeelight.bulb_auto_on == False, "auto_on not 'False'"
    assert yeelight.bulb_effect == "smooth", "effect not 'smooth'"


def test_turn_off_raises():
    # patch get_name_ip_of()
    yeet.get_name_ip_of = lambda _: exec('raise socket.gaierror')
    assert yeet.turn_off("some host") == 1
    yeet.get_name_ip_of = lambda x: (None, "IP", None)

    # assert a bulb instance has not even been created
    assert yeelight.bulb_ip.empty()
    assert yeelight.bulb_auto_on is None
    assert yeelight.bulb_effect is None

    yeelight.turn_off_raise = True
    assert yeet.turn_off("some host") == 2, f"wrong ret code on turn_off_raise"
    yeelight.turn_off_raise = False

    yeet.get_name_ip_of = lambda _: exec('raise socket.herror')
    assert yeet.turn_off("some host") == 1, f"wrong ret code on net err"

    yeet.get_name_ip_of = lambda _: exec('raise Exception')
    with pytest.raises(Exception):
        assert yeet.turn_off("some host"), f"network exception not raised"
