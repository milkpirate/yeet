#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from conftest import yeet


def test_eprint(capsys):
    yeet.eprint("print to stderr")
    stdout, stderr = capsys.readouterr()

    assert stdout == ""
    assert stderr == "print to stderr\n"
