#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

hosts = [
    {
        'ip': '192.168.0.19',
        'port': 55443,
        'capabilities': {
            'bright': '50',
            'color_mode': '1',
            'ct': '2700',
            'fw_ver': '45',
            'hue': '359',
            'id': '0x0000000002dfb19a',
            'model': 'color',
            'name': 'bedroom',
            'power': 'off',
            'rgb': '16711935',
            'sat': '100',
            'support':  'get_prop set_default set_power toggle '
                        'set_bright start_cf stop_cf set_scene cron_add '
                        'cron_get cron_del set_ct_abx set_rgb set_hsv '
                        'set_adjust set_music set_name'
        },
    },
    {
        'ip': '192.168.0.23',
        'port': 55443,
        'capabilities': {
            'bright': '50',
            'color_mode': '1',
            'ct': '2700',
            'fw_ver': '45',
            'hue': '359',
            'id': '0x0000000002dfb2f1',
            'model': 'color',
            'name': 'livingroom',
            'power': 'off',
            'rgb': '16711935',
            'sat': '100',
            'support':  'get_prop set_default set_power toggle '
                        'set_bright start_cf stop_cf set_scene cron_add '
                        'cron_get cron_del set_ct_abx set_rgb set_hsv '
                        'set_adjust set_music set_name'
        },
    },
]

ip_location_lookup = dict(
    zip(
        [h['ip'] for h in hosts],
        ['kitchen', 'livingroom']
    )
)

exp_output = dict(
    machine='IP\tPort\tModel\tPower\tFW version\tID\n192.168.0.19\t55443\tcolor\toff\t45\t0x0000000002dfb19a\n192.168.0.23\t55443\tcolor\toff\t45\t0x0000000002dfb2f1',
    table='+--------------+-------+-------+-------+------------+--------------------+\n|      IP      |  Port | Model | Power | FW version |         ID         |\n+--------------+-------+-------+-------+------------+--------------------+\n| 192.168.0.19 | 55443 | color |  off  |     45     | 0x0000000002dfb19a |\n| 192.168.0.23 | 55443 | color |  off  |     45     | 0x0000000002dfb2f1 |\n+--------------+-------+-------+-------+------------+--------------------+',

    json=dict(
        not_pretty='[{"ip": "192.168.0.19", "port": 55443, "capabilities": {"bright": "50", "color_mode": "1", "ct": "2700", "fw_ver": "45", "hue": "359", "id": "0x0000000002dfb19a", "model": "color", "name": "bedroom", "power": "off", "rgb": "16711935", "sat": "100", "support": "get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name"}}, {"ip": "192.168.0.23", "port": 55443, "capabilities": {"bright": "50", "color_mode": "1", "ct": "2700", "fw_ver": "45", "hue": "359", "id": "0x0000000002dfb2f1", "model": "color", "name": "livingroom", "power": "off", "rgb": "16711935", "sat": "100", "support": "get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name"}}]',
        pretty="[{'capabilities': {'bright': '50',\n                   'color_mode': '1',\n                   'ct': '2700',\n                   'fw_ver': '45',\n                   'hue': '359',\n                   'id': '0x0000000002dfb19a',\n                   'model': 'color',\n                   'name': 'bedroom',\n                   'power': 'off',\n                   'rgb': '16711935',\n                   'sat': '100',\n                   'support': 'get_prop set_default set_power toggle '\n                              'set_bright start_cf stop_cf set_scene cron_add '\n                              'cron_get cron_del set_ct_abx set_rgb set_hsv '\n                              'set_adjust set_music set_name'},\n  'ip': '192.168.0.19',\n  'port': 55443},\n {'capabilities': {'bright': '50',\n                   'color_mode': '1',\n                   'ct': '2700',\n                   'fw_ver': '45',\n                   'hue': '359',\n                   'id': '0x0000000002dfb2f1',\n                   'model': 'color',\n                   'name': 'livingroom',\n                   'power': 'off',\n                   'rgb': '16711935',\n                   'sat': '100',\n                   'support': 'get_prop set_default set_power toggle '\n                              'set_bright start_cf stop_cf set_scene cron_add '\n                              'cron_get cron_del set_ct_abx set_rgb set_hsv '\n                              'set_adjust set_music set_name'},\n  'ip': '192.168.0.23',\n  'port': 55443}]",
    ),
    xml=dict(
        not_pretty='<?xml version="1.0" encoding="UTF-8" ?><bulbs><bulb><ip>192.168.0.19</ip><port>55443</port><capabilities><bright>50</bright><color_mode>1</color_mode><ct>2700</ct><fw_ver>45</fw_ver><hue>359</hue><id>0x0000000002dfb19a</id><model>color</model><name>bedroom</name><power>off</power><rgb>16711935</rgb><sat>100</sat><support>get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name</support></capabilities></bulb><bulb><ip>192.168.0.23</ip><port>55443</port><capabilities><bright>50</bright><color_mode>1</color_mode><ct>2700</ct><fw_ver>45</fw_ver><hue>359</hue><id>0x0000000002dfb2f1</id><model>color</model><name>livingroom</name><power>off</power><rgb>16711935</rgb><sat>100</sat><support>get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name</support></capabilities></bulb></bulbs>',
        pretty='<?xml version="1.0" ?>\n<bulbs>\n\t<bulb>\n\t\t<ip>192.168.0.19</ip>\n\t\t<port>55443</port>\n\t\t<capabilities>\n\t\t\t<bright>50</bright>\n\t\t\t<color_mode>1</color_mode>\n\t\t\t<ct>2700</ct>\n\t\t\t<fw_ver>45</fw_ver>\n\t\t\t<hue>359</hue>\n\t\t\t<id>0x0000000002dfb19a</id>\n\t\t\t<model>color</model>\n\t\t\t<name>bedroom</name>\n\t\t\t<power>off</power>\n\t\t\t<rgb>16711935</rgb>\n\t\t\t<sat>100</sat>\n\t\t\t<support>get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name</support>\n\t\t</capabilities>\n\t</bulb>\n\t<bulb>\n\t\t<ip>192.168.0.23</ip>\n\t\t<port>55443</port>\n\t\t<capabilities>\n\t\t\t<bright>50</bright>\n\t\t\t<color_mode>1</color_mode>\n\t\t\t<ct>2700</ct>\n\t\t\t<fw_ver>45</fw_ver>\n\t\t\t<hue>359</hue>\n\t\t\t<id>0x0000000002dfb2f1</id>\n\t\t\t<model>color</model>\n\t\t\t<name>livingroom</name>\n\t\t\t<power>off</power>\n\t\t\t<rgb>16711935</rgb>\n\t\t\t<sat>100</sat>\n\t\t\t<support>get_prop set_default set_power toggle set_bright start_cf stop_cf set_scene cron_add cron_get cron_del set_ct_abx set_rgb set_hsv set_adjust set_music set_name</support>\n\t\t</capabilities>\n\t</bulb>\n</bulbs>\n',
    ),
)
