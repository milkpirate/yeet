#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
yeelight module mock.

© 2018 Paul Schroeder
"""

import queue
import sys

from types import SimpleNamespace

turn_off_ret = "ok"
turn_on_ret = "ok"

bulb_ip = queue.Queue(maxsize=5)
off_ips = []
on_ips = []
get_properties_ips = []
started_flows = []

bulb_auto_on = None
bulb_effect = None

bulb_prop_set = {}

turn_on_raise = False
turn_off_raise = False
get_properties_raise = False
start_flow_raise = False

count = dict(
    bulb=0,
    turn_on=0,
    turn_off=0,
    get_properties=0,
    start_flow=0,
)


# mock the submodule 'transitions'
def pulse(red, blue, green, duration, brightness):
    return locals()

sys.modules['yeelight.transitions'] = SimpleNamespace(pulse=pulse)
transitions = SimpleNamespace(pulse=lambda *a, **kw: dict(kw))


class Bulb:
    def __init__(self, ip, auto_on=None, effect=None):
        global bulb_ip, bulb_auto_on, bulb_effect

        count["bulb"] += 1
        bulb_ip.put(ip)

        bulb_auto_on = auto_on
        bulb_effect = effect

        self.ip = ip
        self.auto_on = auto_on
        self.effect = effect

    def turn_on(self):
        count["turn_on"] += 1
        on_ips.append(self.ip)

        if turn_on_raise:
            raise Exception

        bulb_prop_set[self.ip]['power'] = 'on'
        return turn_on_ret

    def turn_off(self):
        count["turn_off"] += 1
        off_ips.append(self.ip)

        if turn_off_raise:
            raise Exception

        bulb_prop_set[self.ip]['power'] = 'off'
        return turn_off_ret

    def get_properties(self):
        count["get_properties"] += 1
        get_properties_ips.append(self.ip)

        if get_properties_raise:
            raise Exception

        return bulb_prop_set[self.ip]

    def start_flow(self, flow):
        count["start_flow"] += 1
        started_flows.append(
            dict(
                ip=self.ip,
                flow=flow,
            )
        )

        if start_flow_raise:
            raise Exception


class Flow:
    actions = SimpleNamespace(recover=123)

    def __init__(self, *a, **kw):
        self.props = dict(kw)

