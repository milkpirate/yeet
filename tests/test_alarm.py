#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pytest
import socket
import threading
import time

# mock
import yeelight

# module under test
from conftest import yeet

ret_code = None


def test_alarm_exception_network():
    yeet.get_name_ip_of = lambda _: exec('raise socket.herror')
    assert yeet.alarm('some host') == 1


def test_alarm_exception_properties():
    yeelight.get_properties_raise = True
    assert yeet.alarm('some host') == 2


def test_alarm_exception_start_flow():
    yeelight.start_flow_raise = True
    assert yeet.alarm('some host') == 2


#@pytest.mark.skip("annoying to wait for it to finish")
def test_alarm_exception_flowing():
    yeelight.bulb_prop_set['IP']['flowing'] = '1'

    global ret_code
    ret_code = None

    def thread_wrapper():
        global ret_code
        ret_code = yeet.alarm('IP')

    thread = threading.Thread(target=thread_wrapper)
    thread.start()
    time.sleep(.5)
    assert thread.is_alive(), "alarm not waiting anymore"
    assert ret_code is None

    yeelight.bulb_prop_set['IP']['flowing'] = '0'
    time.sleep(1)
    assert not thread.is_alive(), "alarm still waiting eventhou flow has stopped"
    assert ret_code == 0
