#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pytest

import copy
import shlex
import yeelight

# import test data
from yeelight_sample_discover import hosts, exp_output, ip_location_lookup

# module under test
from conftest import yeet


@pytest.mark.parametrize('help_cmd', ['-h', '--help'])
def test_component_print_help(capsys, help_cmd):
    with pytest.raises(SystemExit):
        cmd = f'yeet.py {help_cmd}'
        cmd = shlex.split(cmd)
        yeet.main(argv=cmd)

    exp_output = yeet.__doc__.strip()
    stdout, stderr = capsys.readouterr()

    assert not stderr, f'stderr not empty: {stderr!r}'
    assert f"{exp_output}\n" == stdout


@pytest.mark.parametrize("cmd, exp_stdout", [
    pytest.param("yeet.py scan", exp_output['table']),
    pytest.param("yeet.py scan --machine", exp_output['machine']),
])
def test_component_scan_machine(capsys, cmd, exp_stdout):
    yeelight.discover_bulbs = lambda: hosts

    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)
    stdout, stderr = capsys.readouterr()

    assert not stderr, f'stderr not empty: {stderr!r}'
    assert f"{exp_stdout}\n" == stdout, f'stdout does not match'


@pytest.mark.parametrize('format', ['xml', 'json'])
@pytest.mark.parametrize('pretty', [False, True])
def test_component_scan_machine(capsys, format, pretty):
    yeelight.discover_bulbs = lambda: hosts

    exp_stdout = exp_output[format]
    exp_stdout = exp_stdout['pretty' if pretty else 'not_pretty']
    exp_stdout += '\n'

    cmd = f'yeet.py scan --{format} {"--pretty" if pretty else ""}'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)
    stdout, stderr = capsys.readouterr()

    assert not stderr, f'stderr not empty: {stderr!r}'
    assert exp_stdout == stdout, f'stdout does not match'


def test_component_off():
    yeelight.discover_bulbs = lambda: hosts
    yeet.get_name_ip_of = lambda ip: ("", ip, "")

    cmd = f'yeet.py off'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)

    for host in hosts:
        assert yeelight.bulb_prop_set[host['ip']]['power'] == 'off'


def test_component_alarm_all():
    yeelight.discover_bulbs = lambda: hosts
    yeet.get_name_ip_of = lambda ip: ("", ip, "")
    exp_ips = [h['ip'] for h in hosts]

    cmd = f'yeet.py alarm'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)

    exp_flow = yeelight.Flow(
        count=9,
        action=yeelight.Flow.actions.recover,
        transitions=yeelight.transitions.pulse(
            red=255,
            blue=0,
            green=0,
            duration=300,
            brightness=100,
        )
    )

    actl_ips = [flow['ip'] for flow in yeelight.started_flows]
    assert exp_ips == actl_ips

    for alarm in yeelight.started_flows:
        flow = alarm['flow']
        assert isinstance(flow, yeelight.Flow)
        assert flow.props == exp_flow.props


@pytest.mark.parametrize('option', ['-H', '--host'])
@pytest.mark.parametrize('input_hosts', [
    pytest.param(hosts),
    pytest.param([hosts[0]]),
    pytest.param([hosts[1]]),
])
def test_component_alarm_single(option, input_hosts):
    yeelight.discover_bulbs = lambda: input_hosts
    yeet.get_name_ip_of = lambda ip: ("", ip, "")
    exp_ips = [h['ip'] for h in input_hosts]

    cmd = f'yeet.py alarm {option if input_hosts else ""} {" ".join(exp_ips)}'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)

    actl_ips = [flow['ip'] for flow in yeelight.started_flows]
    assert exp_ips == actl_ips

    exp_flow = yeelight.Flow(
        count=9,
        action=yeelight.Flow.actions.recover,
        transitions=yeelight.transitions.pulse(
            red=255,
            blue=0,
            green=0,
            duration=300,
            brightness=100,
        )
    )

    for alarm in yeelight.started_flows:
        flow = alarm['flow']
        assert isinstance(flow, yeelight.Flow)
        assert flow.props == exp_flow.props


@pytest.mark.parametrize('action', ['on', 'off'])
@pytest.mark.parametrize('option', ['-H', '--host'])
@pytest.mark.parametrize("pwr_state", [
    pytest.param(['off',    'off']),
    pytest.param(['on',     'off']),
    pytest.param(['off',    'on']),
    pytest.param(['on',     'on']),
])
def test_component_switch_on_off(action, option, pwr_state):
    yeet.get_name_ip_of = lambda ip: ("", ip, "")
    ips = [h['ip'] for h in hosts]

    hosts_mod = copy.deepcopy(hosts)
    for idx, state in enumerate(pwr_state):
        hosts_mod[idx]['capabilities']['power'] = state

    yeelight.discover_bulbs = lambda: hosts_mod

    cmd = f'yeet.py switch {action} {option} {" ".join(ips)}'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)

    for ip in ips:
        assert yeelight.bulb_prop_set[ip]['power'] == action


@pytest.mark.parametrize("pwr_state", [
    pytest.param(['off',    'off']),
    pytest.param(['on',     'off']),
    pytest.param(['off',    'on']),
    pytest.param(['on',     'on']),
])
def test_component_switch_toggle_all(pwr_state):
    yeet.get_name_ip_of = lambda ip: ("", ip, "")
    ips = [h['ip'] for h in hosts]

    exp_prop_set = copy.deepcopy(yeelight.bulb_prop_set)
    hosts_mod = copy.deepcopy(hosts)
    for idx, state in enumerate(pwr_state):
        hosts_mod[idx]['capabilities']['power'] = state
        yeelight.bulb_prop_set[ips[idx]]['power'] = state
        exp_prop_set[ips[idx]]['power'] = 'on' if state == 'off' else 'off'

    yeelight.discover_bulbs = lambda: hosts_mod

    cmd = f'yeet.py switch toggle'
    cmd = shlex.split(cmd)
    yeet.main(argv=cmd)

    assert exp_prop_set == yeelight.bulb_prop_set
