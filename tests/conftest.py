#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""


import pytest

import queue
import sys

import yeelight_mock
sys.modules['yeelight'] = yeelight_mock

sys.path.append('./src')
from yeet import yeet

# import test data
from yeelight_sample_discover import hosts

# @pytest.fixture(scope="function")
# def module_under_test():
#     import src.yeet as test_mod
#     return test_mod


def reset_yeelight_mock():
    yeelight_mock.turn_off_ret = "ok"
    yeelight_mock.turn_on_ret = "ok"

    yeelight_mock.bulb_ip = None
    yeelight_mock.bulb_auto_on = None
    yeelight_mock.bulb_effect = None

    yeelight_mock.turn_on_raise = False
    yeelight_mock.turn_off_raise = False
    yeelight_mock.get_properties_raise = False
    yeelight_mock.start_flow_raise = False

    yeelight_mock.bulb_prop_set = {}
    for host in hosts:
        yeelight_mock.bulb_prop_set['IP'] = dict()
        yeelight_mock.bulb_prop_set['IP']['power'] = 'off'
        yeelight_mock.bulb_prop_set[host['ip']] = dict()
        yeelight_mock.bulb_prop_set[host['ip']]['power'] = 'off'

    yeet.get_name_ip_of = lambda x: (None, "IP", None)

    yeelight_mock.bulb_ip = queue.Queue(maxsize=5)
    yeelight_mock.off_ips = []
    yeelight_mock.on_ips = []
    yeelight_mock.get_properties_ips = []
    yeelight_mock.started_flows = []

    yeet.count = dict(
        bulb=0,
        turn_on=0,
        turn_off=0,
        get_properties=0,
        start_flow=0,
    )


@pytest.fixture(autouse=True, scope='function')
def module_setup_teardown():
    # setup
    yeet.get_name_ip_of = lambda x: (None, "IP", None)
    reset_yeelight_mock()

    # tests are run now
    yield

    # teardown
    reset_yeelight_mock()

#
# @pytest.fixture()
# def max_instance(request):
#     """
#     Create a instance of MAX IC class with mocked littel wire device
#
#     :return: py7219.Max() instance
#     """
#
#     def max_factory(*pargs, **kwargs):
#         kwargs.setdefault('lw', mock_littleWire.device())
#         kwargs.setdefault('common_anode', False)
#         kwargs.setdefault('number_of_digits', 6)
#         max_instance = py7219.Max(*pargs, **kwargs)
#         return max_instance
#
#     return max_factory
#
#
# @pytest.fixture(params=range(256))
# def byte_range(request):
#     return request.param
#
#
# @pytest.fixture(params=iter(vars(py7219.Max.op_code).values()))
# def opcode_range(request):
#     return request.param
#
#
# @pytest.fixture(params=range(16))
# def brightness_range(request):
#     return request.param
#
#
# @pytest.fixture(params=iter([True, False, 0, 1]))
# def boolean_range(request):
#     return request.param
#
#
# @pytest.fixture(params=range(1, 9))
# def pin_range(request):
#     return request.param
#
#
# @pytest.fixture(params=range(1, py7219.Max._max_digits + 1))
# def digit_range(request):
#     return request.param
