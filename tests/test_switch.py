#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pytest
import socket

# mock
import yeelight

# module under test
from conftest import yeet

from pprint import pprint as pp

@pytest.mark.parametrize("des_state, pre_state, exp_state", [
    pytest.param("on",      "off",  "on"),
    pytest.param("on",      "on",   "on"),
    pytest.param("off",     "on",   "off"),
    pytest.param("off",     "off",  "off"),
    pytest.param("toggle",  "on",   "off"),
    pytest.param("toggle",  "off",  "on"),
])
def test_switch_regular(des_state, pre_state, exp_state):
    yeelight.turn_off_ret = "ok"
    yeelight.bulb_prop_set['IP']['power'] = pre_state

    assert yeet.switch("some host", des_state) == 0, "ret code != 0"

    # the following test are essentially the same
    assert yeelight.bulb_prop_set['IP']['power'] == exp_state

    # check that bulb was correctly set up
    assert yeelight.bulb_ip.get() == "IP", "mock ip not 'IP'"
    assert yeelight.bulb_auto_on == True, "auto_on not 'True'"
    assert yeelight.bulb_effect == "smooth", "effect not 'smooth'"


def test_switch_exception_network():
    yeet.get_name_ip_of = lambda _: exec('raise socket.herror')
    assert yeet.switch("some host", 'on') == 1


def test_switch_exception_properties():
    yeelight.get_properties_raise = True
    assert yeet.switch("some host", 'on') == 2


@pytest.mark.parametrize("on_raise, off_raise, cmd, exp_ret", [
    pytest.param(True, False, 'on', 3),
    pytest.param(True, False, 'off', 0),
    pytest.param(False, True, 'on', 0),
    pytest.param(False, True, 'off', 3),
])
def test_switch_exceptions_bulb_turn_on_off(on_raise, off_raise, cmd, exp_ret):
    yeelight.turn_on_raise = on_raise
    yeelight.turn_off_raise = off_raise
    assert yeet.switch("some host", cmd) == exp_ret
