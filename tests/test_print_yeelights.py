#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pytest

import json

# import test data
from yeelight_sample_discover import hosts, exp_output

# module under test
from conftest import yeet


@pytest.mark.parametrize('format', ['xml', 'json'])
@pytest.mark.parametrize('pretty', [False, True])
def test_yeelights_to_fmtstr(format, pretty):
    exp = exp_output[format]
    exp = exp['pretty'] if pretty else exp['not_pretty']

    actl = yeet.yeelights_to_fmtstr(
        hosts,
        format=format,
        pretty=pretty
    )

    assert exp == actl


@pytest.mark.parametrize('format', ['table', 'machine'])
@pytest.mark.parametrize('pretty', [False, True])
def test_yeelights_to_fmtstr_table(format, pretty):
    exp = exp_output[format]

    actl = yeet.yeelights_to_fmtstr(
        hosts,
        format=format,
        pretty=pretty
    )

    assert exp == actl
