#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
YEElight multiTool:

  off       Turns all available Yeelights off (a scan will be performed).
  scan      Performs a scan for available Yeelights and prints some of their info.
  switch    Turns one or more Yeeligts on, off or toggles the actual state.
  alarm     Sends an alarm transistion to one or more Yeelights.

switch/alarm:
    If no host(s) is(/are) given, a scan will be performed and all available
    Yeelights will be alarmed/switched.

Usage:
    yeet.py off
    yeet.py scan [ --machine ]
    yeet.py scan [ --json | --xml ] [ --pretty ]
    yeet.py switch (on | off | toggle) [ --host=<ip>... ]
    yeet.py alarm [ --host=<ip>... ]
    yeet.py -h | --help
    yeet.py -v | --version

Options:
    -m --machine        Print out machine readable.
    -j --json           Print data in JSON format.
    -x --xml            Print data in XML format.
    -p --pretty         Pretty print data.
    -H --host=<ip>...   The yeelight's IPs or hostnames to send the command to.
                        If no host is given a scan will be performed and the
                        command will be send to all the found yeelights.

    -v --version        Show version.
    -h --help           Show this screen.

©2018 Paul Schroeder - paul.schroeder@mail.com
"""

import dicttoxml
import docpie
import json
import pprint
import prettytable
import queue
import socket
import sys
import threading
import time

import yeelight
import yeelight.transitions

from typing import \
    Callable, \
    List, \
    Optional, \
    Tuple

from collections import OrderedDict
from xml.dom.minidom import parseString


def main(
    argv: List[str]=sys.argv
):
    args = docpie.docpie(
        __doc__,
        argv=argv,
        help=True,
        version='yeet 1.0',
    )

    if args['--host']:
        hosts = args['--host']
        ips = hosts
    else:
        hosts = yeelight.discover_bulbs()
        ips = [h['ip'] for h in hosts]

    if args["scan"]:
        format = 'table'

        if args['--machine']:   format = 'machine'
        elif args['--json']:    format = 'json'
        elif args['--xml']:     format = 'xml'

        pretty = args['--pretty']

        print(yeelights_to_fmtstr(hosts, format=format, pretty=pretty))
    elif args["switch"]:
        if args['on']:      des_state = 'on'
        elif args['off']:   des_state = 'off'
        else:               des_state = 'toggle'

        parallelize(ips, switch, des_state)
    elif args["alarm"]:
        parallelize(ips, alarm)
    elif args['off']:
        parallelize(ips, turn_off)


def eprint(*args, **kwargs):
    """
    Print to stderr rather than stdout.

    :param args: The same as of ``print()``
    :param kwargs: The same as of ``print()``

    :return: The same as of ``print()``
    """
    return print(*args, file=sys.stderr, **kwargs)


def get_name_ip_of(
        host: str
) -> Tuple[str, str, str]:
    """
    Retrieve the the IP and the hostname to a given hostname/IP

    :param host: a string with the IP/hostname

    :return: IP, hostname as string

    :raises: socket.gaierror, socket.herror

    >>> get_name_ip_of("127.0.0.1") #doctest: +ELLIPSIS
    ('...', '127.0.0.1', '...')
    >>> get_name_ip_of(1234)
    Traceback (most recent call last):
        ...
    TypeError: given host must be of type str.
    >>> get_name_ip_of("")
    Traceback (most recent call last):
        ...
    ValueError: host can not be falsy.
    """
    if not isinstance(host, str):
        raise TypeError(f"given host must be of type str.")

    if not host:
        raise ValueError(f"host can not be falsy.")

    host_name = socket.getfqdn(host)
    host_ip_v4 = socket.gethostbyname(host)

    host_ip_v6 = None
    try:
        host_ip_v6 = socket.getaddrinfo(host_name, None, socket.AF_INET6)[0][4][0]
    except Exception:   # pragma: no cover
        pass

    return host_name, host_ip_v4, host_ip_v6


def parallelize(
        target_objs: List[object],
        target_func: Callable,
        arg: Optional=None
) -> List[int]:
    """
    Starts a thread for each element ``obj`` in ``target_objs`` , hands \
it with the (result) queue and additions ``args`` if there are some to the \
target function: ``target_func(result_queue, obj[, args])``. This is blocking \
(waits until all threads have terminated) and then returns the (result) queue.

    :param target_objs: The object list you want to operate with \
``taget_func`` on.
    :param target_func: Funtion to be applied on object element in \
``target_objs``.
    :param arg: Additional argument to be passed to ``target_func`` \
(defaul: None)

    :return: Result queue. Is meant to contain returncodes of the threads.

    >>> def tst_fc(*x, rq=None):
    ...     print(*x)
    ...     if rq:
    ...         rq.put({x: 321})
    >>> parallelize(["hello", "world"], tst_fc, "!!!")
    hello !!!
    world !!!
    321
    >>> parallelize(["world"], lambda x, rq=None: print(x))
    world
    0
    """

    threads = {}
    result_queue = queue.Queue(maxsize=len(target_objs))

    for obj in target_objs:
        def wrapper_func(ob, *args, **kwargs):
            name, _, _ = get_name_ip_of(ob)
            eprint(f"Running {target_func.__name__} on {name}...")
            ret = target_func(ob, *args, **kwargs)
            success = "failed" if ret else "succeeded"
            eprint(f"Running {target_func.__name__} on {name} {success}!")

        host_thread = threading.Thread(
            target=wrapper_func,
            args=(obj, arg) if arg else (obj, ),
            kwargs={'rq': result_queue},
        )

        threads[obj] = host_thread
        host_thread.start()

    for host_thread in threads:
        threads[host_thread].join()

    while not result_queue.empty():
        thread_result = result_queue.get()
        threads_obj = list(thread_result)[0]
        threads_return = thread_result[threads_obj]

        if threads_return:
            return threads_return

    return 0


def turn_off(
        host: str,
        rq: Optional[queue.Queue] = None,
) -> int:
    """
    Sends the off command to a Yeelight (host).

    :param host: Yeelight to turn off.
    :param rq: Return queue (to ``put()`` returncode to - default: None)
    :return: Error code (success: 0, failure: >0)

    :raises: ValueError

    >>> turn_off("")
    Traceback (most recent call last):
        ...
    ValueError: host can not be falsy.
    """

    if not host:
        raise ValueError(f"host can not be falsy.")

    try:
        _, ip, _ = get_name_ip_of(host)
    except (socket.gaierror, socket.herror) as exp:
        err_code = 1
        if rq: rq.put(dict(host=err_code))
        return err_code

    try:
        bulb = yeelight.Bulb(ip, auto_on=False, effect="smooth")
        success = bulb.turn_off().lower()
    except Exception:
        err_code = 2
    else:
        err_code = 0 if success == "ok" else 3
    finally:
        if rq: rq.put(dict(host=err_code))
        return err_code


def switch(
        host: str,
        des_state: str="toggle",
        rq: Optional[queue.Queue] = None,
) -> int:
    """
    Send the ``on``, ``off`` or ``toggle`` command to a Yeelight (host).

    :param host: Yeelight to switch
    :param des_state: Desired state (default: ``toogle``).
    :param rq: Return queue (to ``put()`` returncode to - default: None)

    :return: Error code (success: 0, failure: >0)

    :raise: ValueError

    >>> switch("")
    Traceback (most recent call last):
        ...
    ValueError: host can not be falsy.
    >>> switch("some host", des_state="but not a state")
    Traceback (most recent call last):
        ...
    ValueError: des_state is not in ('on', 'off', 'toggle').
    """

    if not host:
        raise ValueError(f"host can not be falsy.")

    if des_state not in ("on", "off", "toggle"):
        raise ValueError(f"des_state is not in ('on', 'off', 'toggle').")

    try:
        _, ip, _ = get_name_ip_of(host)
    except (socket.gaierror, socket.herror) as exp:
        err_code = 1
        if rq: rq.put(dict(host=err_code))
        return err_code

    try:
        bulb = yeelight.Bulb(ip, auto_on=True, effect="smooth")
        actl_pwr = bulb.get_properties()['power']
    except Exception as exp:
        err_code = 2
        if rq: rq.put(dict(host=err_code))
        return err_code

    if des_state == 'on':
        des_pwr = True
    elif des_state == 'off':
        des_pwr = False
    else:   # toggle
        des_pwr = not (actl_pwr == 'on')

    try:
        bulb.turn_on() if des_pwr else bulb.turn_off()
    except Exception as exp:
        err_code = 3
    else:
        err_code = 0
    finally:
        if rq: rq.put(dict(host=err_code))
        return err_code


def yeelights_to_fmtstr(
        yeelights: List[dict],
        format: str='table',
        pretty: bool=False,
) -> str:
    """
    Formats the given list of scanned Yeelights (``yeelight.discover_bulbs``) \
to various formats:

    - **table**: Human readable tabular.
    - **json**: JSON formated.
    - **xml**: XML formated.
    - **machine**: Machine readable.

    **Note:**

    - The ``pretty`` option, if set to ``True`` does prettify JSON and XML \
output.
    - Not all format include the whole set of information. JSON and XML do.

    :param yeelights: List of Yeelights (usually the return of \
``yeelight.discover_bulbs()``)
    :param format: One of the following: ``table``, ``json``, ``xml`` and \
``machine`` (default: ``table``)
    :param pretty: Wether to pretty print or not (default: False)

    :return:

    >>> yeelights_to_fmtstr([])
    '[]'
    >>> yeelights_to_fmtstr([], 'table')
    '[]'
    >>> yeelights_to_fmtstr([], 'json')
    '[]'
    >>> yeelights_to_fmtstr([], 'xml')
    '[]'
    >>> yeelights_to_fmtstr([], 'machine')
    '[]'
    >>> yeelights_to_fmtstr([], "not a format")
    Traceback (most recent call last):
        ...
    ValueError: format must be one of 'table', 'json', 'xml', 'machine'.
    """

    if format not in ("table", "json", "xml", "machine"):
        raise ValueError(f"format must be one of 'table', 'json', 'xml', 'machine'.")

    if not yeelights:
        return '[]'

    if format == "json":
        return pprint.pformat(yeelights) if pretty else json.dumps(yeelights)

    elif format == "xml":
        xml = dicttoxml.dicttoxml(
            yeelights,
            custom_root='bulbs',
            attr_type=False,
            item_func=lambda x: 'bulb',
        )

        return parseString(xml).toprettyxml() if pretty else xml.decode("utf-8")

    elif format == "table" or format == "machine":
        if format == "table":
            table = prettytable.PrettyTable()
            table.align = "l"
        if format == "machine":
            output = []

        for idx, yee in enumerate(yeelights):
            # hostname, _, _ = get_name_ip_of(yee['ip'])

            data = OrderedDict([
                ('IP', yee["ip"]),
                ('Port', yee["port"]),
                # ('Hostname', hostname),
                ('Model', yee["capabilities"]["model"]),
                ('Power', yee["capabilities"]["power"]),
                ('FW version', yee["capabilities"]["fw_ver"]),
                ('ID', yee["capabilities"]["id"]),
                # ('Color Mode', host["capabilities"]["color_mode"]),
            ])

            data_values = list(data.values())
            data_keys = list(data.keys())

            if format == "table":
                if idx == 0: table.field_names = data_keys
                table.add_row(data_values)
            if format == 'machine':
                if idx == 0: output.append('\t'.join(data_keys))
                row = [str(d) for d in data_values]
                row = '\t'.join(row)
                output.append(row)

        if format == "table":
            return table.get_string()
        if format == "machine":
            return '\n'.join(output)


def alarm(
        host: str,
        rq: Optional[queue.Queue] = None,
) -> int:
    """
    Send the an sequence of transitions to a Yeelight (host). Which in this \
case looks like a alarm pattern (blinking read light).

    :param host: Yeelight to switch
    :param rq: Return queue (to ``put()`` returncode to - default: None)

    :return: Error code (success: 0, failure: >0)

    :raise: ValueError

    >>> alarm("")
    Traceback (most recent call last):
        ...
    ValueError: host can not be falsy.
    """

    if not host:
        raise ValueError(f"host can not be falsy.")

    try:
        _, ip, _ = get_name_ip_of(host)
    except (socket.gaierror, socket.herror) as exp:
        err_code = 1
        if rq: rq.put(dict(host=err_code))
        return err_code
    try:
        bulb = yeelight.Bulb(ip, auto_on=True, effect="smooth")
        old_pwr_state = bulb.get_properties()['power']
        bulb.start_flow(
            yeelight.Flow(
                count=9,  # maximal not to exceed quota
                          # (only valid for this flow)
                action=yeelight.Flow.actions.recover,
                transitions=yeelight.transitions.pulse(
                    red=255,
                    blue=0,
                    green=0,
                    duration=300,
                    brightness=100,
                )
            )
        )
        while bulb.get_properties()['flowing'] == '1':
            time.sleep(0.1)  # 100ms
    except Exception as exp:
        err_code = 2
    else:
        if old_pwr_state == 'off':
            bulb.turn_off()
        err_code = 0
    finally:
        if rq: rq.put(dict(host=err_code))
        return err_code


if __name__ == "__main__":  # pragma: no cover
    main(argv=sys.argv)
