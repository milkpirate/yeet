#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

import pathlib

from __about__ import *
from setuptools import setup


# read requirements
def read_requirements(path):
    reqs = pathlib.Path(path).read_text()
    reqs = reqs.splitlines()
    reqs = [r for r in reqs if r]
    reqs = [r for r in reqs if not r.startswith('#')]
    return reqs


setup(
    name=__title__,
    version=__release__,
    packages=['src.yeet'],
    install_requires=read_requirements('./src/requirements.txt'),
    url=__uri__,
    license=__license__,
    author=__author__,
    author_email=__email__,
    description=__summary__
)
